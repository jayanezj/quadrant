class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :name, limit: 200, null: false
      t.boolean :active, default: true
      t.timestamps
    end
  end
end
