class CreateShifts < ActiveRecord::Migration
  def change
    create_table :shifts do |t|
      t.string :name, limit: 100, null: false
      t.integer :hour_id, null: false
      t.boolean :active, default: true
      t.timestamps
    end
  end
end
