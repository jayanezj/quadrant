class CreateHours < ActiveRecord::Migration
  def change
    create_table :hours do |t|
      t.string :name, limit: 100, null: false
      t.boolean :active, default: true
      t.timestamps
    end
  end
end
