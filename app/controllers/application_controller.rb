class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :app_name
   private
     def app_name
       Rails.application.class.to_s.split("::").first
     end
end
