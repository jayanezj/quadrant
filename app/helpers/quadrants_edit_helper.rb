module QuadrantsEditHelper

  def edit_quadrant first
    first = Date.today.beginning_of_month unless first.present?
    last = first.end_of_month
    tmp = first
    output = "<div class=\"well\">"
    output += "<table class=\"table table-condensed table-bordered"
    output += "table-striped warning\"><thead>"
    output += "<tr><th>Lun</th><th>Mar</th><th>Mie</th>"
    output += "<th>Jue</th><th>Vie</th><th>Sab</th><th>Dom</th></tr></thead>"
    output += "<tbody>"
    week = 7
    begin
      if week == 7
        week = 0
        if tmp == first
          output += "<tr>"
          (tmp.wday - 1).times do |i|
            output += "<td>&nbsp;</td>"
            week += 1
          end
        else
          output += "</tr><tr>"
        end
      end
      if [5,6].include? week
        output += "<td><div class=\"well well-sm\" data-content=\"#{tmp.day}\">"
        output += "#{user_weekend(tmp.day)}</td>"
      else
        output += "<td><div class=\"well well-sm\" data-content=\"#{tmp.day}\">"
        output += "#{user_daily(tmp.day)}</td>"
      end
      week += 1
      tmp += 1.day
    end while tmp <= last
    output += "</tr></body></table></div>"
    output += "<input type=\"hidden\" id=\"edit-info\" data-days=\""
    output += "#{last.day}\""
    output += " />"
  end

  def user_daily day
    output = ""
    Employee.present(true).each do |e|

      output += "<div class=\"row\">#{e.name}</div>"

      output += "<div class=\"row\"><div class=\"col-xs-12\">"
      output += "<div class=\"input-group\">"
      output += "<span class=\"input-group-addon\">"
      output += "<input id=\"day-#{day}-usr-#{e.id}-type-1\""
      output += "type=\"checkbox\" /></span>"
      output += "<input type=\"text\" readonly=\"readonly\" value=\"Mañana\""
      output += " class=\"form-control\" /></div></div></div>"

      output += "<div class=\"row\"><div class=\"col-xs-12\">"
      output += "<div class=\"input-group\">"
      output += "<span class=\"input-group-addon\">"
      output += "<input id=\"day-#{day}-usr-#{e.id}-type-2\""
      output += "type=\"checkbox\" /></span>"
      output += "<input type=\"text\" readonly=\"readonly\" value=\"Tarde\""
      output += " class=\"form-control\" /></div></div></div>"

      output += "<div class=\"row\"><div class=\"col-xs-12\">"
      output += "<div class=\"input-group\">"
      output += "<span class=\"input-group-addon\">"
      output += "<input id=\"day-#{day}-usr-#{e.id}-type-4\""
      output += "type=\"checkbox\" /></span>"
      output += "<input type=\"text\" readonly=\"readonly\" value=\"Vacaciones\""
      output += " class=\"form-control\" /></div></div></div>"
    end
    output
  end

  def user_weekend day
    output = ""
    Employee.present(true).each do |e|

      output += "<div class=\"row\">#{e.name}</div>"

      output += "<div class=\"row\"><div class=\"col-xs-12\">"
      output += "<div class=\"input-group\">"
      output += "<span class=\"input-group-addon\">"
      output += "<input id=\"day-#{day}-usr-#{e.id}-type-3\""
      output += "type=\"checkbox\" /></span>"
      output += "<input type=\"text\" readonly=\"readonly\" value=\"Completo\""
      output += " class=\"form-control\" /></div></div></div>"

      output += "<div class=\"row\"><div class=\"col-xs-12\">"
      output += "<div class=\"input-group\">"
      output += "<span class=\"input-group-addon\">"
      output += "<input id=\"day-#{day}-usr-#{e.id}-type-4\""
      output += "type=\"checkbox\" /></span>"
      output += "<input type=\"text\" readonly=\"readonly\" value=\"Vacaciones\""
      output += " class=\"form-control\" /></div></div></div>"
    end
    output
  end
end
