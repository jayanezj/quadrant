class QuadrantMonth < ActiveRecord::Base

  belongs_to :shift
  belongs_to :employee

  scope :qdate, -> (date) { where date: date }
  scope :shift_hour, -> (hour) { joins(:shift).where(:"shifts.hour_id" => hour) }
  # scope :erase_month, -> (month) {
  #   where(:"MONTH(date)" => month.month).where(:"YEAR(date)" => month.year)
  # }

  scope :erase_month, -> (month) {
    smonth = "#{month.month}"
    if month.month < 10
      smonth = "0#{smonth}"
    end
    where("strftime('%m', `date`) = ?", smonth).where(
    "strftime('%Y', `date`) = ?", "#{month.year}")
  }
end
