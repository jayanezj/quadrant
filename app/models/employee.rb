class Employee < ActiveRecord::Base
  has_many :quadrant_months

  scope :present, -> (swift) { where active: swift }
end
